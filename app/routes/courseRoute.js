// khai báo thư viện express
const express = require("express");

// khai báo middleware

const {
    getAllCoures,
    getCoures,
    postCoures,
    putCoures,
    deleteCoures
} = require("../middlewares/courseMiddleware");
const { getAllCourse, getCourseById, createCourse, updateCourse, deleteCourse  } = require("../controllers/courseController");


// tạo ra routes
 const router = express.Router();
 router.use(express.json());


// get all course
 router.get("/course", getAllCourse)

 // get a course
 router.get("/course/:courseid", getCourseById)

 // Create a Courese
 router.post("/course",createCourse)

 // Update a Courese
 router.put("/course/:courseid", updateCourse);

 // delete a Course
 router.delete("/course/:courseid", deleteCourse)

 module.exports = {router};
 