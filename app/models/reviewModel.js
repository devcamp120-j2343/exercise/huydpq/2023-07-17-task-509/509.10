// khai báo thư viện mongoose
const mongoose = require('mongoose');

// hai báo Schema lấy từ thư viện mongoose:
const Schema = mongoose.Schema;

// khai báo một schema
const reviewSchema = new Schema({
    _id: mongoose.Types.ObjectId,
    stars : {
        type: Number,
        default: 0
    },
    note : {
        type: String,
        require: false,
    },
    created_At: {
        type: Date,
        default : Date.now()
    },
    update_At: {
        type: Date,
        default : Date.now()
    }
});

// exports 
module.exports = mongoose.model("review", reviewSchema);